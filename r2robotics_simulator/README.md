# r2robotics_simulator

## Prerequisite

Pour lancer Gazebo avec VMWare : éteindre la machine, aller dans les settings, et dans "Display", décochez "Accelerate 3D graphics"

A ajouter au ~/.bashrc:
``` 
export GAZEBO_MODEL_PATH=/home/r2robotics/r2robotics/catkin_ws/src/
```

```
sudo apt-get install ros-kinetic-xacro
sudo apt-get install ros-kinetic-gazebo-plugins
```

## Troubleshooting

Si Gazebo crash au démarrage (process has died) : source the catkin_ws/devel/setup.bash then write the following command
```
killall gzserver
```

## Launching the simulator

Tu launch only the table environment :
```
roslaunch r2robotics_simulator table2018_world.launch
```

To launch the robot in an empty environment :
```
roslaunch r2robotics_simulator gr_empty_world.launch
```

To launch the robot with the table environment :
```
roslaunch r2robotics_simulator gr_table2018_world.launch
```
