#!/usr/bin/env python
import rospy
import xml.etree.ElementTree as ET
import math
import rospkg
import os

def writeLink():
    link = ET.Element('link')  
    link.set('name', 'base_link')
    inertia = ET.SubElement(link, 'xacro:default_inertial')
    inertia.set('mass', '100')
    return link

def writeBox(parent, X, Y, Z, theta, x, y, z):
    visual = ET.SubElement(parent, 'visual') 
    collision = ET.SubElement(parent, 'collision') 
    for p in [visual, collision]:
        origin = ET.SubElement(p, 'origin')
        xyz = ' '.join([str(i) for i in [X,Y,Z]])
        origin.set('xyz', xyz)
        rpy = ' '.join([str(i) for i in [0,0,theta]])
        origin.set('rpy', rpy)
        geometry = ET.SubElement(p, 'geometry')
        box = ET.SubElement(geometry, 'box')
        size = ' '.join([str(i) for i in [x,y,z]])
        box.set('size', size)
    
def writeCylinder(parent, X, Y, Z, r, h):
    visual = ET.SubElement(parent, 'visual') 
    collision = ET.SubElement(parent, 'collision') 
    for p in [visual, collision]:
        origin = ET.SubElement(p, 'origin')
        xyz = ' '.join([str(i) for i in [X,Y,Z]])
        origin.set('xyz', xyz)
        geometry = ET.SubElement(p, 'geometry')
        cylinder = ET.SubElement(geometry, 'cylinder')
        cylinder.set('radius', str(r))
        cylinder.set('length', str(h))

def writeToFile(link):
    # create a new XML file with the results
    rospack = rospkg.RosPack()
    path1 = rospack.get_path('r2robotics_gr_description')
    filepath = os.path.join(path1, 'robots', "generated_gr.urdf.xacro")
    myfile = open(filepath, "w")  
    print "\nOPENING FILE !!!\n"
    
    myfile.write("""<?xml version="1.0"?>
<robot name="macroed" xmlns:xacro="http://ros.org/wiki/xacro">

  <xacro:property name="pi" value="3.1415" />
  <xacro:property name="inertial_value" value="100.0" />

  <xacro:macro name="default_inertial" params="mass">
    <inertial>
      <mass value="${mass}" />
      <inertia ixx="${inertial_value}" ixy="0.0" ixz="0.0" 
      iyy="${inertial_value}" iyz="0.0" izz="${inertial_value}" />
    </inertial>
  </xacro:macro>
""")
    mydata = ET.tostring(link)  
    myfile.write(mydata)  
    myfile.write("""
  <gazebo reference="base_link">
    <material>Gazebo/PurpleGlow</material>
    <maxVel>0.0</maxVel>
    <minDepth>0.00</minDepth>
  </gazebo>
  
  <gazebo>
    <plugin name="object_controller" filename="libgazebo_ros_planar_move.so">
      <commandTopic>cmd_vel</commandTopic>
      <odometryTopic>odom</odometryTopic>
      <odometryFrame>odom</odometryFrame>
      <odometryRate>20.0</odometryRate>
      <robotBaseFrame>base_link</robotBaseFrame>
    </plugin>
  </gazebo>
</robot>""")
    print "CLOSING FIle !\n"
    myfile.close()

def getParamAndDivide(name):
    x = rospy.get_param(name)
    return x/1000.0

def readParamAndWrite():
    link = writeLink()
    nb_parts = rospy.get_param("number_robot_parts")
    for i in range(nb_parts):
        prefix = 'robot_part_' + str(i) + '_'
        parttype = rospy.get_param(prefix + 'type')
        if parttype == 'cylinder':
            X = getParamAndDivide(prefix + 'x')
            Y = getParamAndDivide(prefix + 'y')
            Z = getParamAndDivide(prefix + 'z')
            r = getParamAndDivide(prefix + 'r')
            h = getParamAndDivide(prefix + 'h')
            writeCylinder(link, X, Y, Z, r, h)
        elif parttype == 'box':
            X = getParamAndDivide(prefix + 'x')
            Y = getParamAndDivide(prefix + 'y')
            Z = getParamAndDivide(prefix + 'z')
            t = math.radians( rospy.get_param(prefix + 'theta') )
            x = getParamAndDivide(prefix + 'size_x')
            y = getParamAndDivide(prefix + 'size_y')
            z = getParamAndDivide(prefix + 'size_z')
            writeBox(link, X, Y, Z, t, x, y, z)
    writeToFile(link)


if __name__ == '__main__':
    rospy.init_node('talker', anonymous=True)
    readParamAndWrite()