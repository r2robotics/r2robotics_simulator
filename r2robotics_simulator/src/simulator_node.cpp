#include "ros/ros.h"
#include <gazebo_msgs/LinkStates.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Quaternion.h>

using namespace std;

class Simulator
{
    public:
        Simulator( ros::NodeHandle node );
        void loop();
    private:
        gazebo_msgs::LinkStates states;
        void linkStatesCallback(const gazebo_msgs::LinkStates feedback);
        ros::Publisher simu_clocked_topic;
        ros::Subscriber simu_topic;
};

Simulator::Simulator( ros::NodeHandle n )
{
  simu_clocked_topic = n.advertise<gazebo_msgs::LinkStates>("simulation_pose", 1000);
  simu_topic = n.subscribe("gazebo/link_states", 1000, &Simulator::linkStatesCallback, this);
}

void Simulator::loop()
{
    simu_clocked_topic.publish(states);
}

void Simulator::linkStatesCallback(const gazebo_msgs::LinkStates feedback)
{
    states.name = feedback.name;
    states.pose = feedback.pose;
    states.twist = feedback.twist;

    // int i = -1;
    // bool stop = false;
    // while(i < feedback->name.size() && !stop)
    // {
    //     i++;
    //     if (feedback->name[i] == "gr::base_link")
    //         stop = true;
    // }
    // double x = feedback->pose[i].position.x; 
    // double y = feedback->pose[i].position.y;
    // double yaw = asin(sqrt(feedback->pose[i].orientation.x * feedback->pose[i].orientation.x
    //             + feedback->pose[i].orientation.y * feedback->pose[i].orientation.y
    //             + feedback->pose[i].orientation.z * feedback->pose[i].orientation.z))*2;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "simulator");
  ros::NodeHandle n;

  Simulator simu( n );

  double freq;
  if ( n.getParam("simulator_frequency", freq) ) {
    ROS_INFO("Simulator rate = %f ", freq);
  }
  else {
    ROS_ERROR("Failed to get Simulator rate !");
  }

  ROS_INFO("Node Simulator started !");

  ros::Rate loop_rate(freq);
  int count = 0;
  while (ros::ok())
  {
    simu.loop();
    ros::spinOnce();
    loop_rate.sleep();
    ++count;
  }

}